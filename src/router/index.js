import { createRouter,createWebHistory } from "vue-router";
const router=createRouter({
    history:createWebHistory(),
    routes:[
        {
            component:()=>import('../components/gold.vue'),
            name:'gold',
            path:'/gold'
        },
        {
            component:()=>import('../components/host.vue'),
            name:'host',
            path:'/host'
        },
        {
            component:()=>import('../components/length.vue'),
            name:'length',
            path:'/length'
        },
        {
            component:()=>import('../components/watchnum.vue'),
            name:'watchnum',
            path:'/watchnum'
        },
        {
            //实现路由重定向，当进入网页时，路由自动跳转到/student路由
            redirect:'/gold',
            path:'/'
        }
    ]
})
export default router